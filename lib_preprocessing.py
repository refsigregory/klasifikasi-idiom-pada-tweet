# Text Prepocessing
import re, string
import nltk

#nltk.download()
#nltk.download('punkt')
from nltk.tokenize import word_tokenize

class TextPrep:

    def caseFolding(self, text):
        text = text.lower()  # huruf kecil
        #remove link
        text = re.sub(r'http\S+', '', text)
        text = re.sub(r'https\S+', '', text)

        # remove username
        text = re.sub('@\S+', '', text)
        # remove username
        text = re.sub('#\S+', '', text)

        text = re.sub(r"\d+", " ", text)  # angka
        text = text.replace("_", " ")
        text = text.translate(str.maketrans("", "", string.punctuation))  # tanda baca
        text = text.strip()  # whitespace

        return re.sub('[^a-zA-Z\ ]+', ' ', text) # simbol

    def tokenizing(self, text):
        text = text.translate(str.maketrans('', '', string.punctuation)).lower() # case folding
        #tokens = text.split()  # cara sederhana
        tokens = nltk.tokenize.word_tokenize(text)
        return tokens

    def filtering(self, text):
        from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
        from nltk.tokenize import word_tokenize
        factory = StopWordRemoverFactory()
        stopword = factory.create_stop_word_remover()
        text = text.translate(str.maketrans('', '', string.punctuation)).lower()
        stop = stopword.remove(text)
        tokens = nltk.tokenize.word_tokenize(stop)
        return tokens

    def stemming(self, text):

        from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()
        text = stemmer.stem(text)
        return text


    def run(self, text):
        #print(self.filtering(text))
        text = self.caseFolding(self,text)
        arr_text = self.tokenizing(self,text)

        # filtering #

        # stemming #
        text = [] # reset
        for row in arr_text:
            text.append(row)

        return text

#EXAMPLE#
#prep = TextPrep()
#print(prep.run("aku boleh ngomong dalam bahasa apapun, kan? Ngga cuman bahasa inggris? Johnny : Iya, terserah kamu saja Chenle"))