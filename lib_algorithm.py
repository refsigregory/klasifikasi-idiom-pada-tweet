import itertools
import collections

class NB:
    def __string__(self, idiom, tweet):
        '''idiom = [
            {
                "idiom": "Habis manis, sepah dibuang. Seperti itu peribahasa yang tepat untuk menggambarkan diriku setiap saat",
                "label": "sedih"},
            {"idiom": "Nasi sudah menjadi bubur. Yang sudah terlanjur takkan bisa kembali lagi", "label": "sedih"},
            {
                "idiom": "Masih ingat pelajaran bahasa indonesia? Dimana disitu ada peribahasa bagaikan pungguk merindukan bulan",
                "label": "sedih"},
            {"idiom": "mati satu tumbuh seribu. Peribahasa yang cocok untuk jerawat di wajah saat ini",
             "label": "marah"},
            {
                "idiom": "Dalam sebuah pelajaran tentang peribahasa daring, inilah contoh konkret dari peribahasa air beriak tanda tak dalam",
                "label": "jijik"},
            {
                "idiom": "Bila tidak tahu terhadap sesuatu, lebih baik diam. Sangat relevan sekali di era keterbukaan informasi begini",
                "label": "jijik"}
        ]'''

        label = []
        for row in idiom:
            label.append(row['kategori'])

        daftar_label = collections.Counter(label).items()
        for row in daftar_label:
            peluang_kelas = row[1] / len(idiom)

        # hitung jumlah kata pada setiap data #
        jumlah_kata = []
        # num_words = [len(sentence['idiom'].split()) for sentence in idiom]
        num_words = []  # jumlah setiap kata
        all_words = []  # kata dalam setiap kategori
        for row in idiom:
            num_words.append([row['kategori'], len(row['idiom'].split())])  # hitung jumlah kata disetiap label
            all_words.append([row['kategori'], row['idiom'].replace(",", "").replace(".", "").replace("?", "").lower().split(" "),
                              len(row['idiom'].split())])  # ambil kata di setiap label

        def takeFirst(elem):
            return elem[0]

        sorted_data = sorted(num_words, key=takeFirst)  # mengurutkan data berdasarkan label
        sorted_all_words = sorted(all_words, key=takeFirst)  # mengurutkan data berdasarkan label

        def extract_key(v):
            return v[0]

        # itertools.groupby needs data to be sorted first
        sorted_data = sorted(sorted_data, key=extract_key)
        merge_sorted_data = [
            [k, [x[1] for x in g]]
            for k, g in itertools.groupby(sorted_data, extract_key)  # menggabungkan berdasarkan label
        ]
        for row in merge_sorted_data:
            jumlah_kata.append([row[0], sum(row[1])])  # menghitung jumlah kata yang sudah digabungkan

        # itertools.groupby needs data to be sorted first
        sorted_all_words = sorted(sorted_all_words, key=extract_key)
        merge_sorted_all_words = [
            [k, [x[1] for x in g]]
            for k, g in itertools.groupby(sorted_all_words, extract_key)  # menggabungkan berdasarkan label
        ]
        setiap_kata_dalam_label = []
        for row in merge_sorted_all_words:
            new_list = []
            for merged_list in row[1]:
                new_list = new_list + merged_list
            setiap_kata_dalam_label.append([row[0], new_list, len(new_list)])

        # mengambil semua kata unik dalam data traning #
        unique_data = []
        all_idiom_with_label = []
        for row in idiom:
            unique_data.append(row['idiom'])
            all_idiom_with_label.append([row['idiom'], row['kategori']])

        # pecah kata dengan label masing-masing
        all_kata_with_label = []
        for row in all_idiom_with_label:
            for row2 in row[0].replace(",", "").replace(".", "").replace("?", "").lower().split(" "):
                all_kata_with_label.append([row2, row[1]])

        unique_data = ' '.join(unique_data).replace(",", "").replace(".", "").replace("?", "").lower()  # sambung semua data
        unique_data = unique_data.split(" ")  # pecah lagi
        all_data = unique_data
        unique_data = collections.Counter(unique_data).items()  # buat jadi unik
        kata_unik = []
        for row in unique_data:
            kata_unik.append(row[0])  # bersihkan hasil dan pindahkan ke list baru
        jumlah_kata_unik = len(kata_unik)

        # menggunakan data testing dengan cara menghitung peluang setiap kata #
        daftar_peluang = []
        data_testing = tweet.split(" ")

        for kata in data_testing:
            for list_label_kata in setiap_kata_dalam_label:
                jml_traning = 1
                jml_kata = list_label_kata[1].count(kata)
                peluang = (jml_kata + jml_traning) / (list_label_kata[2] + jumlah_kata_unik)  # hitung peluang
                daftar_peluang.append([kata, str(peluang), list_label_kata[0]])
                print(
                    "(" + str(jml_kata) + " + " + str(jml_traning) + ")" + " / " + "(" + str(list_label_kata[2]) + " + " + str(
                        jumlah_kata_unik) + ")")
                print("=" + str([kata, str(peluang), list_label_kata[0]]))

        def filterbyvalue(seq, value):
            for el in seq:
                if el[2] == value:
                    return el

        def multiplyList(myList):
            # Multiply elements one by one
            result = 1
            for x in myList:
                result = result * x
            return result

        probabilitas_peluang = []
        for row in daftar_label:
            peluang_kelas = row[1] / len(idiom)
            _peluang = []
            print(row[0])
            for el in daftar_peluang:
                if el[2] == row[0]:
                    _peluang.append(float(el[1]))
                    print(str(el[0]) + ":")
                    print("" + str(el[1]))
            total_peluang = peluang_kelas * multiplyList(_peluang)
            print("= " + str(peluang_kelas) + " x " + str(multiplyList(_peluang)))
            print()
            probabilitas_peluang.append([row[0], total_peluang])

        # ambil prubalitas peluang tertinggi
        tertinggi = 0
        index = 0
        for row in probabilitas_peluang:
            if row[1] > tertinggi:
                tertinggi = row[1]
                index = row[0]
        print(index)
        print(tertinggi)
        return index

# data traning #
idiom = [
    {"idiom": "Habis manis, sepah dibuang. Seperti itu peribahasa yang tepat untuk menggambarkan diriku setiap saat",
     "label": "sedih"},
    {"idiom": "Nasi sudah menjadi bubur. Yang sudah terlanjur takkan bisa kembali lagi", "label": "sedih"},
    {"idiom": "Masih ingat pelajaran bahasa indonesia? Dimana disitu ada peribahasa bagaikan pungguk merindukan bulan",
     "label": "sedih"},
    {"idiom": "mati satu tumbuh seribu. Peribahasa yang cocok untuk jerawat di wajah saat ini", "label": "marah"},
    {
        "idiom": "Dalam sebuah pelajaran tentang peribahasa daring, inilah contoh konkret dari peribahasa air beriak tanda tak dalam",
        "label": "jijik"},
    {
        "idiom": "Bila tidak tahu terhadap sesuatu, lebih baik diam. Sangat relevan sekali di era keterbukaan informasi begini",
        "label": "jijik"}
]
# data testing #
tweet = "PR ku seperti peribahasa mati satu tumbuh seribu"
# tweet = "Habis madu sepah dibuang"

#start = NB
#print(start(idiom, tweet))