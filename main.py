import pymongo
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import tweepy
import json
import pandas as pd
import csv
import re
from textblob import TextBlob
import string
import preprocessor as p
import os
import time
import math
import collections, itertools

from flask import Flask
from flask import render_template, request, jsonify, redirect
from flask_cors import CORS
from bson.objectid import ObjectId
from lib_preprocessing import TextPrep as prep
from lib_algorithm import NB as NaiveBayes
from lib_ngram import ngram

APP_NAME = "Klasifikasi Idiom Tweet"

# Twitter credentials
# Obtain them from your twitter developer account
consumer_key = 'bc00NuWgLMstzvw08o17zJYgZ'
consumer_secret = '91rpQ9htQAtmsYQXoXz2485NNK5BDg0JKaJFwYKc8JMhI1hcZ5'
access_key = '396558965-UgMtusvTL2HfSyApS70Dbg3cdp53biGo0Pkiudr6'
access_secret = 'FBvfGCNTnaupud3ZEiSyydOada6WoYul62KWLYN4fzKbw' # Pass your twitter credentials to tweepy via its OAuthHandlerauth = OAuthHandler(consumer_key, consumer_secret)
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth)

# mongodb://127.0.0.1:27017
client = pymongo.MongoClient("127.0.0.1:27017")
db = client['klasifikasiIdiomTweet']
tweets = db['tweet_collection']
tweet_collection = db['tweets']
jargon_collection = db['jargon']
idiom_collection = db['idiom_collection']
preprocessing_collection = db['preprocessing']
testing_collection = db['testing']
training_collection = db['training']
result_collection = db['result']

def getTweet():
    public_tweets = api.home_timeline()
    tweet_list = []
    for tweet in public_tweets:
        #print(tweet)
        tweet_list.append({'username': tweet.user.name,'name': tweet.user.screen_name, 'text': tweet.text, 'time': tweet.created_at})
    print(tweet_list)
    return tweet_list
def searchTweet(query = 'halo', max_tweets = 100):
    searched_tweets = []
    last_id = -1
    while len(searched_tweets) < max_tweets:
        count = max_tweets - len(searched_tweets)
        try:
            new_tweets = api.search(q=query, count=count, max_id=str(last_id - 1))
            if not new_tweets:
                break
            searched_tweets.extend(new_tweets)
            last_id = new_tweets[-1].id
        except tweepy.TweepError as e:
            # depending on TweepError.code, one may want to retry or wait
            # to keep things simple, we will give up on an error
            break
    return searched_tweets
def saveTweet(data):
    results = tweet_collection.insert_many(data)
    return results
def savePreProcess(data):
    results = preprocessing_collection.insert_many(data)
    return results
def saveDataTesting(data):
    results = testing_collection.insert_many(data)
    return results
def saveDataTraining(data):
    results = training_collection.insert_many(data)
    return results
def getSavedTweet():
    query = tweet_collection.find({}, {'text': 1})
    return query
def saveIdiom(data):
    results = idiom_collection.insert_many(data)
    return results
def saveJargon(data):
    results = jargon_collection.insert_many(data)
    return results
def saveDataResult(data):
    results = result_collection.insert_many(data)
    return results

app = Flask(__name__)
CORS(app)

@app.route("/")
def main():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": ""
    }
    return render_template('index.html', data=data)

@app.route("/daftar_tweet", methods=["GET"])
def daftar_tweet():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": tweets.find()
    }
    return render_template('get_tweet.html', data=data)

@app.route("/tweet", methods=["GET"])
def tweet():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": tweet_collection.find()
    }
    return render_template('tweet.html', data=data)

@app.route("/jargon", methods=["GET"])
def jargon():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": jargon_collection.find()
    }
    return render_template('jargon.html', data=data)

@app.route("/insert_jargon", methods=["POST"])
def insert_jargon():
    if request.method == 'POST':
        saveJargon([{'jargon': request.form.get('jargon'),'actual_word': request.form.get('actual_word')}])
    return redirect('/jargon')

@app.route("/delete_jargon", methods=["GET"])
def delete_jargon():
    if request.method == 'GET':
        id = request.args.get('id')
        jargon_collection.delete_one({'_id': ObjectId(id)})
    return redirect('/jargon')

@app.route("/reset_jargon", methods=["GET"])
def reset_jargon():
    jargon_collection.drop()
    return redirect('/jargon')


@app.route("/get_tweet", methods=["GET"])
def get_tweet():
    tweet_list = []
    if request.method == 'GET':
        if request.args.get('q'):
            query = request.args.get('q')
            max = int(request.args.get('max'))
            #print(searchTweet(query))
            for tweet in searchTweet(query, max):
                tweet_list.append({'username': tweet.user.name, 'name': tweet.user.screen_name, 'text': tweet.text,
                                   'time': tweet.created_at})
            if len(tweet_list) >= 0:
                #tweet_collection.drop()
                saveTweet(tweet_list)
    else:
        print("pencarian kosong")

    return redirect('/tweet')

@app.route("/filter_tweet", methods=["GET"])
def filter_tweet():
    for row in list(tweets.find()):
        for row_idiom in list(idiom_collection.find()):
            n = len(row_idiom['idiom'].split())
            tw = row['tweet'].lower().replace(".","").replace(",","").replace("@","").replace("!","")
            idi = row_idiom['idiom'].lower().replace(".","").replace(",","").replace("@","").replace("!","")
            if ngram.check(ngram,tw, idi, n):
                saveTweet([{'username': row['username'], 'tweet': row['tweet']}])
                print("ADA")
                continue;
            else:
                print(".")
        print("================")

    return redirect('/tweet')

@app.route("/start_divide", methods=["GET"])
def start_divide():
    if request.method == 'GET':
        train = []
        count = len(list(testing_collection.find()))
        input_persen = int(request.args.get('persen'))
        persen_training = input_persen / 100
        max = math.floor(count * persen_training)
        i = 1;
        for row in list(testing_collection.find()):
            if i <= max:
                print(row)
                train.append({'tweet': row['tweet'], 'label_manual': row['label_manual'], 'persentasi': ((100-input_persen)/100)})
                testing_collection.delete_one({'_id': row['_id']})
            i = i+1
        if(len(train)) > 0:
            saveDataTraining(train)
    return redirect('/training')

@app.route("/start_preprocess", methods=["GET"])
def start_preprocess():
    tweet_list = []
    if request.method == 'GET':
        id = request.args.get('id')
        data_idiom = idiom_collection.find({}, {"idiom": 1, "kategori": 1, "_id": 0})
        idiom = list(data_idiom)

        #tweet = data[0]['tweet']

        for row in list(tweet_collection.find()):
            tweet = row['tweet']
            #print(tweet)
            result = prep.run(prep,tweet)
            #print(result)
            #idiom_one = {'idiom': request.form.get('idiom'), 'makna': request.form.get('makna'),
            #            'kategori': request.form.get('kategori')}
            #savePreProcess([idiom_one])
            if len(result) > 0:
                prepdata = preprocessing_collection.find({"tweet": tweet})
                if len(list(prepdata)) == 0:  # save jika belum ada
                    #idiom_one = {'id_tweet': id, 'tweet': tweet, 'after_processing': " ".join(result), 'label_otomatis': "", 'label_manual': ""}
                    #saveDataTesting([idiom_one])

                    normalisasi = []
                    str_normal = prep.caseFolding(prep,tweet)
                    print("From: " + str_normal)
                    for row in jargon_collection.find():
                        str_normal = str_normal.replace(row['jargon'], row['actual_word'])
                    print("Result: " + str_normal)

                    result_normalisasi = " ".join(prep.tokenizing(prep,str_normal))
                    print("Normalisasi: " + result_normalisasi)

                    preprocess = {'tweet': tweet, 'case_folding': prep.caseFolding(prep,tweet), 'normalisasi_kalimat': result_normalisasi, 'token': prep.tokenizing(prep,str_normal), 'result': result_normalisasi}
                    print(preprocess)
                    savePreProcess([preprocess])

                    idiom_one = {'id_tweet': id,'tweet': tweet, 'after_processing': result_normalisasi, 'label_otomatis': "", 'label_manual': ""}
                    saveDataTesting([idiom_one])

        return redirect('/preprocess')

@app.route("/start_classify", methods=["GET"])
def start_classify():
    id = request.args.get('id')
    data = testing_collection.find({"_id": id})
    # prepdata = preprocessing_collection.find({"id_tweet": id})
    data_training = training_collection.find({}, {"tweet": 1, "label_manual": 1, "_id": 0})
    idiom = list(data_training) #idiom sudah diganti data training

    tweet_list = []
    for row_tweet in list(testing_collection.find()):

        try:
            tweet = row_tweet['after_processing']
        except KeyError:
            tweet = row_tweet['tweet']

        label = []

        for row in idiom:
            label.append(row['label_manual'])

        daftar_label = collections.Counter(label).items()
        for row in daftar_label:
            peluang_kelas = row[1] / len(idiom)

        # hitung jumlah kata pada setiap data #
        jumlah_kata = []
        # num_words = [len(sentence['idiom'].split()) for sentence in idiom]
        num_words = []  # jumlah setiap kata
        all_words = []  # kata dalam setiap kategori
        for row in idiom:
            num_words.append([row['label_manual'], len(row['tweet'].split())])  # hitung jumlah kata disetiap label
            all_words.append([row['label_manual'], row['tweet'].replace(",", "").replace(".", "").replace("?", "").lower().split(" "),
                              len(row['tweet'].split())])  # ambil kata di setiap label

        def takeFirst(elem):
            return elem[0]

        sorted_data = sorted(num_words, key=takeFirst)  # mengurutkan data berdasarkan label
        sorted_all_words = sorted(all_words, key=takeFirst)  # mengurutkan data berdasarkan label

        def extract_key(v):
            return v[0]

        # itertools.groupby needs data to be sorted first
        sorted_data = sorted(sorted_data, key=extract_key)
        merge_sorted_data = [
            [k, [x[1] for x in g]]
            for k, g in itertools.groupby(sorted_data, extract_key)  # menggabungkan berdasarkan label
        ]
        for row in merge_sorted_data:
            jumlah_kata.append([row[0], sum(row[1])])  # menghitung jumlah kata yang sudah digabungkan

        # itertools.groupby needs data to be sorted first
        sorted_all_words = sorted(sorted_all_words, key=extract_key)
        merge_sorted_all_words = [
            [k, [x[1] for x in g]]
            for k, g in itertools.groupby(sorted_all_words, extract_key)  # menggabungkan berdasarkan label
        ]
        setiap_kata_dalam_label = []
        for row in merge_sorted_all_words:
            new_list = []
            for merged_list in row[1]:
                new_list = new_list + merged_list
            setiap_kata_dalam_label.append([row[0], new_list, len(new_list)])

        # mengambil semua kata unik dalam data traning #
        unique_data = []
        all_idiom_with_label = []
        for row in idiom:
            unique_data.append(row['tweet'])
            all_idiom_with_label.append([row['tweet'], row['label_manual']])

        # pecah kata dengan label masing-masing
        all_kata_with_label = []
        for row in all_idiom_with_label:
            for row2 in row[0].replace(",", "").replace(".", "").replace("?", "").lower().split(" "):
                all_kata_with_label.append([row2, row[1]])

        unique_data = ' '.join(unique_data).replace(",", "").replace(".", "").replace("?", "").lower()  # sambung semua data
        unique_data = unique_data.split(" ")  # pecah lagi
        all_data = unique_data
        unique_data = collections.Counter(unique_data).items()  # buat jadi unik
        kata_unik = []
        for row in unique_data:
            kata_unik.append(row[0])  # bersihkan hasil dan pindahkan ke list baru
        jumlah_kata_unik = len(kata_unik)

        # menggunakan data testing dengan cara menghitung peluang setiap kata #
        daftar_peluang = []
        data_testing = tweet.split(" ")

        for kata in data_testing:
            for list_label_kata in setiap_kata_dalam_label:
                jml_traning = 1
                jml_kata = list_label_kata[1].count(kata)
                peluang = (jml_kata + jml_traning) / (list_label_kata[2] + jumlah_kata_unik)  # hitung peluang
                daftar_peluang.append([kata, str(peluang), list_label_kata[0]])
                print(
                    "(" + str(jml_kata) + " + " + str(jml_traning) + ")" + " / " + "(" + str(list_label_kata[2]) + " + " + str(
                        jumlah_kata_unik) + ")")
                print("=" + str([kata, str(peluang), list_label_kata[0]]))

        def filterbyvalue(seq, value):
            for el in seq:
                if el[2] == value:
                    return el

        def multiplyList(myList):
            # Multiply elements one by one
            result = 1
            for x in myList:
                result = result * x
            return result

        probabilitas_peluang = []
        for row in daftar_label:
            peluang_kelas = row[1] / len(idiom)
            _peluang = []
            print(row[0])
            for el in daftar_peluang:
                if el[2] == row[0]:
                    _peluang.append(float(el[1]))
                    print(str(el[0]) + ":")
                    print("" + str(el[1]))
            total_peluang = peluang_kelas * multiplyList(_peluang)
            print("= " + str(peluang_kelas) + " x " + str(multiplyList(_peluang)))
            print()
            probabilitas_peluang.append([row[0], total_peluang])

        # ambil prubalitas peluang tertinggi
        tertinggi = 0
        index = 0
        for row in probabilitas_peluang:
            if row[1] > tertinggi:
                tertinggi = row[1]
                index = row[0]
        print(index)
        print(tertinggi)

        hasil_ova = ""

        otomatis = index.lower()

        for row in idiom:
            manual = row['label_manual'].lower()
            if (manual == otomatis):
                if index == "senang":
                    hasil_ova = "True Positive"
                elif index == "kaget":
                    hasil_ova = "True Positive"
                elif index == "netral":
                    hasil_ova = "True Positive"
                elif index == "marah":
                    hasil_ova = "True Negative"
                elif index == "sedih":
                    hasil_ova = "True Negative"
                elif index == "takut":
                    hasil_ova = "True Negative"
                elif index == "jijik":
                    hasil_ova = "True Negative"
            else:
                if (manual == "senang" or manual == "kaget" or manual == "netral") and (otomatis == "marah" or otomatis == "sedih" or otomatis == "takut" or otomatis == "jijik"):
                    hasil_ova = "False Positive"

                if (otomatis == "senang" or otomatis == "kaget" or otomatis == "otomatis") and (
                        manual == "marah" or manual == "sedih" or manual == "takut" or manual == "jijik"):
                    hasil_ova = "False Negative"

            '''if(row['label_manual'].lower() == index):
                if index == "senang":
                    hasil_ova = "True Positive"
                elif index == "kaget":
                    hasil_ova = "True Positive"
                elif index == "netral":
                    hasil_ova = "True Positive"
                elif index == "marah":
                    hasil_ova = "True Negative"
                elif index == "sedih":
                    hasil_ova = "True Negative"
                elif index == "takut":
                    hasil_ova = "True Negative"
                elif index == "jijik":
                    hasil_ova = "True Negative"
            else:
                if index == "senang":
                    hasil_ova = "False Positive"
                elif index == "kaget":
                    hasil_ova = "False Positive"
                elif index == "netral":
                    hasil_ova = "False Positive"
                elif index == "marah":
                    hasil_ova = "False Negative"
                elif index == "sedih":
                    hasil_ova = "False Negative"
                elif index == "takut":
                    hasil_ova = "False Negative"
                elif index == "jijik":
                    hasil_ova = "False Negative"
                    '''


        myquery = {"after_processing": tweet}
        newvalues = {"$set": {"label_otomatis": index, "ova": hasil_ova}}
        testing_collection.update_one(myquery, newvalues)

    return redirect('/testing')

@app.route("/start_validation", methods=["GET"])
def start_validation():
    '''jumlah_data = 0
    for row in testing_collection.find():
        print(row)
        jumlah_data += 1

    # kelompkan berdasarlan label ototmati

    jumlah_label = 7
    label_senang = []
    label_kaget = []
    label_netral = []
    label_marah = []
    label_sedih = []
    label_takut = []
    label_jijik = []

    nilai_prediksi = jumlah_label / jumlah_data

    print(nilai_prediksi)

    # jumlah per label
    jumlah_senang = 0
    #for row in label_senang:
    #    print(row)
    #    jumlah_senang += 1
    #jumlah_senang = label_senang / jumlah_data
'''

    '''
    type = ""
    date = ""
    jumlah_testing = 0
    jumlah_training = 0
    persen = 0
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    for row in testing_collection.find():
        jumlah_testing += 1
        if(row['kategori_otomatis'] == "positif" and row['kategori_manual'] == "positif"):
            print("TRUE POSITIVE (TP)")
            tp = tp+1
        elif(row['kategori_otomatis'] == "positif" and row['kategori_manual'] == "negatif"):
            print("FALSE POSITIVE (FP)")
            fp = fp+1
        elif (row['kategori_otomatis'] == "negatif" and row['kategori_manual'] == "negatif"):
            print("TRUE NEGATIVE (TN)")
            tn = tn+1
        elif (row['kategori_otomatis'] == "negatif" and row['kategori_manual'] == "positif"):
            print("FALSE NEGATIVE (FN)")
            fn = fn+1
        else:
            print("ERROR")
        print("========")
   for row in testing_collection.find():
        jumlah_training += 1
        persen = row['persentasi']

    print("TP: " + str(tp))
    print("TN: " + str(tn))
    print("FP: " + str(fp))
    print("FN: " + str(fn))

    accuracy = (tp+tn) / (tp+tn+fp+fn)
    precision = tp / (tp+fp)
    recall = tp / (tp+fn)
    fmeasure = 2 * ((precision * recall) / ( precision + recall ))

    print("Accuracy: " + str(accuracy))
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F-Measure: " + str(fmeasure))

    from datetime import datetime
    saveDataResult([{'date': datetime.today().strftime('%Y-%m-%d %H:%M:%S'), 'testing': jumlah_testing,  'training': jumlah_training, 'persentasi': persen, 'true_positive': tp, 'true_negative': tn, 'false_positive': fp, 'false_negative': fn, 'accuracy': accuracy, 'precision': precision, 'recall': recall, 'f_measure': fmeasure}])

    return redirect('/validation')
    '''

    type = ""
    date = ""
    jumlah_testing = 0
    jumlah_training = 0
    persen = 0
    tp = 0
    tn = 0
    fp = 0
    fn = 0

    for row in testing_collection.find():
        jumlah_testing += 1
        if row['ova'] == "True Positive":
            tp = tp + 1
        elif row['ova'] == "False Positive":
            fp = fp+1
        elif row['ova'] == "True Negative":
            tn = tn + 1
        elif row['ova'] == "False Negative":
            fn = fn + 1
        else:
            print("ERROR")
        print("========")

    for row in training_collection.find():
        jumlah_training += 1
        persen = row['persentasi']

    print("TP: " + str(tp))
    print("TN: " + str(tn))
    print("FP: " + str(fp))
    print("FN: " + str(fn))

    try:
        accuracy = (tp + tn) / (tp + tn + fp + fn)
    except ZeroDivisionError:
        accuracy = 0

    try:
        precision = tp / (tp + fp)
    except ZeroDivisionError:
        precision = 0

    try:
        recall = tp / (tp + fn)
    except ZeroDivisionError:
        recall = 0

    try:
        fmeasure = 2 * ((precision * recall) / (precision + recall))
    except ZeroDivisionError:
        fmeasure = 0

    print("Accuracy: " + str(accuracy))
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F-Measure: " + str(fmeasure))

    from datetime import datetime
    saveDataResult([{'date': datetime.today().strftime('%Y-%m-%d %H:%M:%S'), 'testing': jumlah_testing,
                     'training': jumlah_training, 'persentasi': persen, 'true_positive': tp, 'true_negative': tn,
                     'false_positive': fp, 'false_negative': fn, 'accuracy': accuracy, 'precision': precision,
                     'recall': recall, 'f_measure': fmeasure}])

    return redirect('/validation')

@app.route("/testing", methods=["GET"])
def show_testing():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": testing_collection.find()
    }
    print(data['output'])
    return render_template('testing.html', data=data)

@app.route("/training", methods=["GET"])
def show_training():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": training_collection.find()
    }
    print(data['output'])
    return render_template('training.html', data=data)


@app.route("/preprocess", methods=["GET"])
def preprocess():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": preprocessing_collection.find()
    }
    print(data['output'])
    return render_template('preprocess.html', data=data)

@app.route("/validation", methods=["GET"])
def validation():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": result_collection.find()
    }
    print(data['output'])
    return render_template('validation.html', data=data)

@app.route("/reset_validation", methods=["GET"])
def reset_validation():
    result_collection.drop()
    return redirect('/validation')

@app.route("/delete_validation", methods=["GET"])
def delete_validation():
    tweet_list = []
    if request.method == 'GET':
        id = request.args.get('id')
        result_collection.delete_one({'_id': ObjectId(id)})
    return redirect('/validation')


@app.route("/delete_tweet", methods=["GET"])
def delete_tweet():
    tweet_list = []
    if request.method == 'GET':
        id = request.args.get('id')
        tweet_collection.delete_one({'_id': ObjectId(id)})
    return redirect('/get_tweet')

@app.route("/reset_tweet", methods=["GET"])
def reset_tweet():
    tweet_collection.drop()
    return redirect('/tweet')

@app.route("/reset_tweets", methods=["GET"])
def reset_tweets():
    tweets.drop()
    return redirect('/get_tweet')


@app.route("/idiom", methods=["GET"])
def idiom():
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": idiom_collection.find({}, {'idiom': 1, 'makna': 1})
    }
    print( idiom_collection.find({}, {'idiom': 1})[1])
    return render_template('idiom.html', data=data)

@app.route("/import_idiom", methods=["GET"])
def import_idiom():
    file = ""
    data = {
        "title": APP_NAME,
        "file": "",
        "dataset": "",
        "output": ""
    }
    #if request.method == 'GET':
    if request.values.get('action') == "upload":
        # file = request.files["data"]
        #file = request.args.get('file')
        dataset = pd.read_csv("data/dataset.csv")
        data = {
            "title": APP_NAME,
            "file": "",
            "dataset": "",
            "output": dataset.values
        }


    return render_template('import_idiom.html', data=data)

@app.route("/save_testing_label", methods=["POST"])
def save_testing_label():
    if request.method == 'POST':
        id = request.form.get('id')
        label_manual = request.form.get('label_manual')

        myquery = {"_id": ObjectId(id)}
        if label_manual == "senang" or label_manual == "netral" or label_manual == "kaget":
            kategori = "positif"
        else:
            kategori = "negatif"
        newvalues = {"$set": {"label_manual": label_manual}}

        testing_collection.update_one(myquery, newvalues)
    return redirect('/testing#'+id)

@app.route("/save_training_label", methods=["POST"])
def save_training_label():
    if request.method == 'POST':
        id = request.form.get('id')
        label_manual = request.form.get('label_manual')

        myquery = {"_id": ObjectId(id)}
        newvalues = {"$set": {"label_manual": label_manual}}

        training_collection.update_one(myquery, newvalues)
    return redirect('/training#'+id)


@app.route("/save_import_idiom", methods=["GET"])
def save_import_idiom():
    # file = request.files["data"]
    #file = request.args.get('file')
    dataset = pd.read_csv("data/dataset.csv")
    for row in dataset.values:
        idiom_one = {'idiom': row[0], 'makna': row[1]}
        saveIdiom([idiom_one])
    return redirect('/idiom')

@app.route('/upload', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save("data/dataset.csv")
        return redirect('/import_idiom?action=upload')
    return redirect('/import_idiom')

@app.route("/insert_idiom", methods=["POST"])
def insert_idiom():
    if request.method == 'POST':
        idiom_one = {'idiom': request.form.get('idiom'),'makna': request.form.get('makna'), 'kategori': request.form.get('kategori')}
        saveIdiom([idiom_one])
    return redirect('/idiom')

@app.route("/insert_testing", methods=["POST"])
def insert_testing():
    if request.method == 'POST':
        testing_one = {'tweet': request.form.get('tweet'), 'after_processing': request.form.get('tweet'), 'label_otomatis': "", 'label_manual': "", 'ova': ""}
        saveDataTesting([testing_one])
    return redirect('/testing')

@app.route("/delete_idiom", methods=["GET"])
def delete_idiom():
    if request.method == 'GET':
        id = request.args.get('id')
        idiom_collection.delete_one({'_id': ObjectId(id)})
    return redirect('/idiom')

@app.route("/reset_idiom", methods=["GET"])
def reset_idiom():
    idiom_collection.drop()
    return redirect('/idiom')

@app.route("/reset_preprocess", methods=["GET"])
def reset_preprocess():
    preprocessing_collection.drop()
    return redirect('/preprocess')

@app.route("/reset_testing", methods=["GET"])
def reset_testing():
    testing_collection.drop()
    return redirect('/testing')

@app.route("/reset_training", methods=["GET"])
def reset_training():
    training_collection.drop()
    return redirect('/training')

@app.route("/reset_daftar_tweet", methods=["GET"])
def reset_daftar_tweet():
    tweets.drop()
    return redirect('/daftar_tweet')

if __name__ == '__main__':
    #app.run()
    app.run(debug=True)


'''

printer_a = {'printer_name': 'X Printer Co', 'printer_model': 231901, 'price': 250.00}
printer_a = {'printer_name': 'X Printer Co', 'printer_model': 231901, 'price': 250.00}
results = printer_collection.insert_many([printer_a])
'''