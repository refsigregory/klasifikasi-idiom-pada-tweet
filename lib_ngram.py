import nltk
from nltk.util import ngrams
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class ngram:
    def compare_ngrams(self, trigrams1, trigrams2):
        trigrams1 = list(trigrams1)
        trigrams2 = list(trigrams2)
        common = []
        for gram in trigrams1:
            if gram in trigrams2:
                common.append(gram)
        return common

    def check(self, text1, text2, n):
      #n = 2
      if( n != 1):
          n = n - 1
      min = 1
      trigrams1 = ngrams(text1.lower().split(), n)
      trigrams2 = ngrams(text2.lower().split(), n)

      common = self.compare_ngrams(self,trigrams1, trigrams2)

      if len(common) >= min:
        return True
      else:
        return False
'''
kamus_idiom = [["Otak kosong nyaring bunyinya", "MARAH"], ["Ada udang dibalik batu", "MARAH"]]
input_text = 'Saya tidak suka orang yang otak kosong nyaring bunyinya'
# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()
text = stemmer.stem(input_text)

ngram = ngram()
for row in kamus_idiom:
    if ngram.check(text, row[0]):
        print(row[1])
    else:
        print("NETRAL")
'''
